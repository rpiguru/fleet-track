#!/bin/bash
sleep 20
echo "FleetTrack autostart"
until /home/pi/fleettrack/main.py; do
	echo "FleetTrack crashed with exit code $?. Restarting...">&2
	sleep 1
done
