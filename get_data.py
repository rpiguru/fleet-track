import urllib
import re


def macBeacons(debug_on, imei):
    link = "http://www.calitalia.cloud/assets/php/db/getMacAddr.php?imei=" + str(imei)
    f = urllib.urlopen(link)
    myfile = f.read()
    ble_addr = re.split(',', myfile)
    my_devices = []
    tmp = {}
    if debug_on:
        print('IMEI BLE OPERATORS')
    for addr in ble_addr:
        tmp['addr'] = addr
        my_devices.append(addr)
        if debug_on:
            print 'IMEI = ' + addr
    return my_devices


def sensorScale(debug_on, imei):
    link = "http://www.calitalia.cloud/assets/php/db/getSensorsValue.php?imei=" + str(imei)
    f = urllib.urlopen(link)
    myfile = f.read()
    mycsv = re.split(',', myfile)
    my_val = []
    if debug_on:
        print('SENSORS SCALE')
    for val in mycsv:
        my_val.append(val)
        if debug_on:
            print 'VAL = ' + val
    return my_val


def getBlocco(debug_on, imei):
    link = "http://www.calitalia.cloud/assets/php/db/getBlocco.php?imei=" + str(imei)
    print(link)
    f = urllib.urlopen(link)
    myfile = f.read()
    if debug_on:
        print 'BLOCCO = ' + myfile
    return myfile
