import subprocess
import time
import serial
import os
import signal

SERIALPORT = "/dev/ttyUSB0"
BAUDRATE = 19200


def SwitchSerialMode():
    ko = 'No devices in default mode found. Nothing to do. Bye!'
    ok = 'Run lsusb to note any changes. Bye!'

    print("Switching Huawei E303 into Serial Modem Mode...")
    out_a = subprocess.check_output(
        'sudo usb_modeswitch -v 0x12d1 -p 0x1f01 -V 0x12d1 -P 0x1001 '
        '-M "55534243000000000000000000000611060000000000000000000000000000"',
        shell=True)
    out_b = subprocess.check_output(
        'sudo usb_modeswitch -v 0x12d1 -p 0x14fe -V 0x12d1 -P 0x1001 '
        '-M "55534243000000000000000000000611060000000000000000000000000000"',
        shell=True)

    if out_a.find(ko) >= 0:
        print('Huawei E303 doesn\'t switch')
    if out_b.find(ko) >= 0:
        print('Huawei T@ttoo doesn\'t switch')
    if out_a.find(ok) >= 0:
        print('Huawei E303 switched')
    if out_b.find(ok) >= 0:
        print('Huawei T@ttoo switched')


def Connect():
    subprocess.Popen(['sudo', 'wvdial', 'Vodafone'])
    time.sleep(45)
    # out_a = subprocess.check_output('ping -c 4 www.google.it',shell=True)
    # print(out_a)


def Disconnect():
    subprocess.Popen(['sudo', 'killall', 'wvdial'])
    time.sleep(1)
    subprocess.Popen(['sudo', 'fuser', '-k', '/dev/ttyUSB0'])
    for line in os.popen("ps ax|grep wvdial| grep -v grep"):
        print('line = ' + str(line))
        fields = line.split()
        pid = fields[0]
        os.kill(int(pid), signal.SIGKILL)


def _read_line(ser, tout=1):
    lsl = len(os.linesep)
    line_buffer = []
    try:
        start_time = time.time()
        while True:
            next_char = ser.read(1)
            if next_char == "\r":
                break
            line_buffer.append(next_char)
            if len(line_buffer) >= lsl and line_buffer[-lsl:] == list(os.linesep):
                break
            if time.time() - start_time > tout:  # timeout
                line_buffer = []
                break
        return ''.join(line_buffer)
    except Exception as e:
        print('Failed to read data from the serial port - {}'.format(e))
        return 0


def GetIMEI():
    imei = 0
    try:
        ser = serial.Serial(port=SERIALPORT, baudrate=BAUDRATE, bytesize=serial.EIGHTBITS,
                            parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
                            timeout=2, xonxoff=False, rtscts=False, dsrdtr=False,
                            writeTimeout=5)
        # ser.bytesize = serial.EIGHTBITS       # number of bits per bytes
        # ser.parity = serial.PARITY_NONE       # set parity check: no parity
        # ser.stopbits = serial.STOPBITS_ONE    # number of stop bits
        # # ser.timeout = None                  # block read
        # # ser.timeout = 0                     # non-block read
        # ser.timeout = 2                       # timeout block read
        # ser.xonxoff = False                   # disable software flow control
        # ser.rtscts = False                    # disable hardware (RTS/CTS) flow control
        # ser.dsrdtr = False                    # disable hardware (DSR/DTR) flow control
        # ser.writeTimeout = 0                  # timeout for write
    except serial.SerialException:
        print('Get Imei Serial Port Error...')
        return 0

    if ser.isOpen():
        print("Serial port " + ser.portstr + " opened")
        try:
            ser.flushInput()  # flush input buffer, discarding all its contents
            ser.flushOutput()  # flush output buffer, aborting current output

            # print("AT+CGSN")
            ser.write("AT+CGSN\r\n")
            time.sleep(1)
            timeout = 0
            while True:
                timeout = timeout + 1
                response = _read_line(ser)
                # print("response = " + str(response))
                # print("read data: " + response)
                # print("len data: " + str(len(response)))
                if response.isdigit() and len(response) >= 15:
                    print("found IMEI - {}".format(response))
                    imei = response
                    break

                if response == "OK\r\n" or timeout >= 100:
                    print("response OK or timeout")
                    break
            ser.close()

        except Exception as e:
            print("error communicating...: " + str(e))
            return 0
    else:
        print("cannot open serial port")
        return 0
    return imei


if __name__ == '__main__':
    print(SwitchSerialMode())

    try:
        while True:
            print(GetIMEI())
            time.sleep(1)
    except KeyboardInterrupt:
        pass
