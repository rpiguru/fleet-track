import urllib2
import RPi.GPIO as GPIO


GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(22, GPIO.OUT)


def GetKeyPos(imei):
    ret = 0
    url = "http://www.calitalia.cloud/assets/php/db/getKeyPos.php?imei=" + str(imei)
    print(url)

    sock = urllib2.urlopen(url)
    htmlSource = sock.read()
    sock.close()
    print('len(htmlSource)=' + str(len(htmlSource)))
    print('htmlSource = ' + str(htmlSource))
    if len(htmlSource) <= 2:
        ret = 1
    print('ret value KeyPos = ' + str(ret))
    return ret


def setKeyPos(val):
    if val == 0:
        print('setKeyPos 0')
        GPIO.output(22, GPIO.LOW)
    else:
        print('setKeyPos 1')
        GPIO.output(22, GPIO.HIGH)
