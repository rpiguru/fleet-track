import time
import serial
import os
import re
import subprocess

SERIALPORT = "/dev/ttyUSB0"
BAUDRATE = 115200

conn = 0


def init():
    print("init")
    try:
        ser = serial.Serial(SERIALPORT, BAUDRATE)
        ser.bytesize = serial.EIGHTBITS  # number of bits per bytes
        ser.parity = serial.PARITY_NONE  # set parity check: no parity
        ser.stopbits = serial.STOPBITS_ONE  # number of stop bits
        # ser.timeout = None     #block read
        # ser.timeout = 0        #non-block read
        ser.timeout = 2  # timeout block read
        ser.xonxoff = False  # disable software flow control
        ser.rtscts = False  # disable hardware (RTS/CTS) flow control
        ser.dsrdtr = False  # disable hardware (DSR/DTR) flow control
        ser.writeTimeout = 30  # timeout for write

    except serial.SerialException:
        print('init Serial Port Error...')
        return 0

    if ser.isOpen():
        print("init Serial port " + ser.portstr + " opened")
        try:
            ser.flushInput()
            time.sleep(1)
            ser.flushOutput()
            time.sleep(1)
            print("ser.write(\"at+cops=0, 2\r\n\")")
            try:
                ser.write("at+cops=0, 2\r\n")
            except Exception as e:
                print("error ser.write location init" + str(e))
            print("DONE ser.write(\"at+cops=0, 2\r\n\")")
            time.sleep(1)
            timeout = 0
            while True:
                timeout += 1
                response = ser.readline()
                if response == "OK\r\n":
                    print('OK at+cops=0, 2')
                    break
                if timeout >= 100:
                    break

            time.sleep(1)
            ser.write("at+creg=2\r\n")
            time.sleep(1)
            timeout = 0
            while True:
                timeout += 1
                response = ser.readline()
                if response == "OK\r\n":
                    print('OK at+creg=2')
                    break
                if timeout >= 100:
                    break
            ser.close()

        except Exception as e:
            print("error " + str(e))
    else:
        print("couldn't open port")


def getMccMnc():
    print("getMccMnc")
    try:
        ser = serial.Serial(SERIALPORT, BAUDRATE)
        ser.bytesize = serial.EIGHTBITS  # number of bits per bytes
        ser.parity = serial.PARITY_NONE  # set parity check: no parity
        ser.stopbits = serial.STOPBITS_ONE  # number of stop bits
        # ser.timeout = None     #block read
        # ser.timeout = 0        #non-block read
        ser.timeout = 2  # timeout block read
        ser.xonxoff = False  # disable software flow control
        ser.rtscts = False  # disable hardware (RTS/CTS) flow control
        ser.dsrdtr = False  # disable hardware (DSR/DTR) flow control
        ser.writeTimeout = 0  # timeout for write
    except serial.SerialException:
        print('getMccMnc Serial Port Error...')
        return []

    cops = 0
    if ser.isOpen():
        print("getMccMnc Serial port " + ser.portstr + " opened")
        try:
            ser.flushInput()  # flush input buffer, discarding all its contents
            ser.flushOutput()  # flush output buffer, aborting current output
            # print("*************************")
            # print("at+cops=0,2")
            ser.write("at+cops=0,2\r\n")
            time.sleep(1)
            timeout = 0
            while True:
                timeout = timeout + 1
                response = ser.readline()
                print("read data: " + response)
                if response == "OK\r\n":
                    print('OK at+cops=0,2')
                    break
                if timeout >= 15:
                    return
            # print("*************************")
            time.sleep(1)
            print("at+cops?")
            ser.write("at+cops?\r\n")
            time.sleep(1)
            timeout = 0
            while True:
                timeout = timeout + 1
                response = ser.readline()
                print("read data: " + response)
                if response.startswith('+COPS:'):
                    cops = response.split('"')
                    print("+COPS : " + str(cops))
                if response == "OK\r\n":
                    print('OK at+cops?')
                    break
                if timeout >= 15:
                    return
        except Exception as e:
            print("error communicating...: " + str(e))
            return []
    else:
        print("cannot open serial port")
        return []

    print('len(cops) = ' + str(len(cops)))
    print('cops = ' + str(cops))
    if len(cops) > 1:
        print('len(cops) > 1 True')
        print('cops[1] = ' + str(cops[1]))
        mcc = int(cops[1][:3], 10)
        mnc = int(cops[1][3:], 10)
        print('MCC = ' + str(mcc))
        print('MNC = ' + str(mnc))
        return [mcc, mnc]
    else:
        print("cops missing")
        return []


#############################################################


def getLacCid():
    print("getLacCid")
    try:
        ser = serial.Serial(SERIALPORT, BAUDRATE)
        ser.bytesize = serial.EIGHTBITS  # number of bits per bytes
        ser.parity = serial.PARITY_NONE  # set parity check: no parity
        ser.stopbits = serial.STOPBITS_ONE  # number of stop bits
        # ser.timeout = None     #block read
        # ser.timeout = 0        #non-block read
        ser.timeout = 2  # timeout block read
        ser.xonxoff = False  # disable software flow control
        ser.rtscts = False  # disable hardware (RTS/CTS) flow control
        ser.dsrdtr = False  # disable hardware (DSR/DTR) flow control
        ser.writeTimeout = 0  # timeout for write
    except serial.SerialException:
        print('getLacCid Serial Port Error...')
        return []

    if ser.isOpen():
        print("getLacCid Serial port " + ser.portstr + " opened")
        try:
            ser.flushInput()
            ser.flushOutput()
            # print("at+creg=2")
            ser.write("at+creg=2\r\n")
            time.sleep(1)
            timeout = 0
            while True:
                timeout = timeout + 1
                response = ser.readline()
                #    print("read data: " + response)
                if response == "OK\r\n":
                    print('OK at+creg = 2')
                    break
                if timeout >= 15:
                    return
            # print("*************************")
            time.sleep(1)
            # print("at+creg?")
            ser.write("at+creg?\r\n")
            time.sleep(1)
            timeout = 0
            while True:
                timeout = timeout + 1
                response = ser.readline()
                print("read data: " + response)
                if response.startswith('+CREG:'):
                    creg = response.split('"')
                    print('+CREG:' + str(creg))
                if response == "OK\r\n":
                    print('OK at+creg?')
                    break
                if timeout >= 15:
                    return
            # print("*************************")
            ser.close()

        except Exception as e:
            print("error communicating...: " + str(e))
            return []
    else:
        print("cannot open serial port")
    try:
        if len(creg) > 3:
            cid = int(creg[3], 16)
        lac = int(creg[1], 16)
        return [cid, lac]
    except:
        return []


#####################################################


def getWifiAP():
    list_ap = []
    p = re.compile(ur'(?:[0-9a-fA-F]:?){12}')
    try:
        raw_output = os.popen("sudo iw dev wlan0 scan | grep -E '^BSS|signal'").read()
        out = raw_output.split("\n")
        if len(out) > 2:
            for i in range(len(out) / 2):
                mac = re.findall(p, out[i * 2])
                signal_strength = int(float(out[(i * 2) + 1].split(" ")[1]))
                list_ap.append([mac[0], signal_strength])
    except Exception as e:
        print("getWifiAP error  " + str(e))

    finally:
        # print list_ap
        return list_ap


#############################################################


def getLocation():
    init()
    print('INIT DONE')
    mccmnc = getMccMnc()
    print('getMccMnc DONE' + str(mccmnc))
    count = 0
    while count < 3 and mccmnc == []:
        mccmnc = getMccMnc()
        count += 1

    data = '{'
    if len(mccmnc) == 2:
        data += '"homeMobileCountryCode": '
        data += str(mccmnc[0])
        data += ', "homeMobileNetworkCode": '
        data += str(mccmnc[1]) + ','
    data += '"radioType": "gsm",'
    data += '"carrier": "Vodafone",'
    data += '"considerIp": "false",'
    data += '"cellTowers": ['
    laccid = getLacCid()
    count = 0
    while len(laccid) != 2 and count < 3:
        laccid = getLacCid()
        count += 1
    if (len(mccmnc) == 2) and (len(laccid) == 2):
        data += '{'
        data += '"cellId": ' + str(laccid[0]) + ','
        data += '"locationAreaCode": ' + str(laccid[1]) + ','
        data += '"mobileCountryCode": ' + str(mccmnc[0]) + ','
        data += '"mobileNetworkCode": ' + str(mccmnc[1]) + '}'

    data += '], '

    wifiAP = getWifiAP()
    data += '"wifiAccessPoints": ['
    for i in range(len(wifiAP)):
        data += '{'
        data += '"macAddress": ' + '"' + wifiAP[i][0] + '"' + ','
        data += '"signalStrength": ' + str(wifiAP[i][1]) + ','
        data += '"signalToNoiseRatio": 0' + '}'
        if i != (len(wifiAP) - 1):
            data += ', '
    data += ']'

    data += ' }'
    print(data)
    # se non c'e' una connessione attiva , lancia wvdial
    '''
    response = os.system("ping -c 1 www.google.it")
    if response != 0:
        print('Connect...')
        Connect()
    else:
        print('Already Connected')
    '''
    Connect()
    try:
        f = open('data.json', 'w')
        f.write(data)
        f.close()
        ss = os.popen(
            'curl -d @data.json -H "Content-Type: application/json" -i '
            '"https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyB1LL1WwvvULCLdMwWe9tvoStoMNivzwus"').read()
        print('ss = ' + str(ss))
        return ss.split("Transfer-Encoding: chunked\r\n\r\n")[1]

    except:
        print('Location couldn\'t be found')
        return "no location detected"


def Connect():
    global conn
    conn = subprocess.Popen(['sudo', 'wvdial', 'Vodafone'])
    print('Connection Location...')
    time.sleep(70)
    print('OK')
    print('conn = ', conn.pid)


def Disconnect():
    print('Disconnect location')
    global conn
    try:
        if conn.poll() is None:
            try:
                print('conn = ' + str(conn.pid))
                subprocess.Popen.terminate(conn)
            except:
                print('No connection avaible to terminate')
            try:
                print('conn = ' + str(conn.pid))
                subprocess.Popen.kill(conn)
            except:
                print('No connection avaible to disconnect')
    except:
        print('No conn.poll()')
    '''
    subprocess.Popen(['sudo', 'killall', 'wvdial'])
    time.sleep(1)
    subprocess.Popen(['sudo', 'fuser', '-k','/dev/ttyUSB0']) 
    for line in os.popen("ps ax|grep wvdial| grep -v grep"):
        print('line = ' + str(line))
        fields = line.split()
        pid = fields[0]
        os.kill(int(pid), signal.SIGKILL)
    '''
