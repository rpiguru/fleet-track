#!/usr/bin/python

import location
import huawei_e303
import time
import sensors_calibrazione
import keypos
import RPi.GPIO as GPIO
import temp
from datetime import datetime
import json
import copy
import os
import zlib
import uploadFile
from bluepy.btle import Scanner, DefaultDelegate
import get_data
import traceback


debug = True
nminutiupload = 2

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(27, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)  # Lampeggio Led
GPIO.setup(17, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)  # Level
GPIO.setup(20, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)  # Accensione

# VARIABILI GLOBALI

imei = 0
my_devices = []
new_ble = 0
date_start_op = {}
date_stop_op = {}
mac_op = {}
nop = 0
t1 = datetime.now()
nlamp = 0
turnOnFlag = False

dataacquisizione = {}
trazione = {}
asp1 = {}
asp2 = {}
spazz1 = {}
spazz2 = {}
vbatt = {}
abatt = {}

ninvii = 0
turnOn_date = []
turnOff_date = []

tmp = {}

n = 0

sending = False  # se true non fa eseguire addLamp


# per non fare aggiornale le variabili globali
# che vengono inviate


# Lettura Bluethoot
# Se trova un beacon presente nella lista my_devices
# E se la macchina e accesa
# E se non era gia' stato rilevato un beacon
#
# setta le seguenti variabili
# new_ble = 1
# date_start_op[nop] = time.time()
# mac_op[nop] = dev.addr
class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        global nop
        global no_ble
        global new_ble
        global date_start_op
        global date_stop_op
        global mac_op
        global my_devices
        global turnOnFlag

        print(str(dev.addr))

        for d in my_devices:
            print('d = ' + str(d))
            try:
                if d.lower() == dev.addr.lower():
                    print('FOUND')
                    if turnOnFlag and new_ble == 0:
                        print('nop= ' + str(nop))
                        print('FOUND B = ' + str(dev.addr))
                        new_ble = 1
                        date_start_op[nop] = time.time()
                        mac_op[nop] = dev.addr
            except Exception as er:
                print('Failed to parse discovered device - {}'.format(traceback.format_exc(er)))


# Quando il led si accende incremento la variabile globale nlamp
# e salvo l'istante del'accensione
# se sono passati piu' di 3 secondi dall'ultima accensione resetto nlamp
def addLamp(_input):
    global t1
    global nlamp
    global sending

    print('LAMP pin(GPIO{}) went HIGH.'.format(_input))

    if sending:
        return
    t = datetime.now() - t1
    delta = t.seconds
    print('Lamp time = ' + str(delta))
    if delta >= 3:
        print('Reset nlamp')
        t1 = datetime.now()
        nlamp = 1
    else:
        t1 = datetime.now()
        nlamp += 1
    print('addLamp -> nlamp = ' + str(nlamp))


def ReadLevel():
    return GPIO.input(17)


# Funzione call back dell'accensione della macchina
# Quando viene accesa la prima volta la variabile globale turnOnFlag
# deve essere settata a 0
def turnKey(_input):
    global new_ble
    global nop
    global turnOnFlag

    print('TurnKEY START callback')
    stato = GPIO.input(_input)

    if stato == 1:
        print('Turn On')
        if turnOnFlag is False:
            print('Memorized Turn On')
            turnOn_date.append(time.time())
            turnOnFlag = True

    if stato == 0:
        print('Turn Off')
        if turnOnFlag is True:
            print('Memorized Turn Off')
            turnOnFlag = False
            turnOff_date.append(time.time())
            if new_ble != 0:
                new_ble = 0
                date_stop_op[nop] = time.time()
                nop = nop + 1
    print('TurnKEY END callback')


# Funzione che mi ritorna una stringa contenente la data attuale
def GetDateTime():
    now = datetime.now()
    return now.strftime("%Y-%m-%d %H:%M:%S")


def CreateJsonData(currdate, _imei, lat, _long, _temp, level, error):
    global dataacquisizione
    global trazione
    global asp1
    global asp2
    global spazz1
    global spazz2
    global vbatt
    global abatt
    global date_start_op
    global date_stop_op
    global mac_op
    global turnOn_date
    global turnOff_date

    lst = {'sd': currdate, 'i': _imei, 'lt': lat, 'lg': _long, 'tp': _temp, 'lv': level, 'er': error, 'd': {}, 'o': {},
           'ks': {}, 'ke': {}}

    d = {}
    print('Creating JSON data - {}'.format(dataacquisizione))
    for i, dt in enumerate(dataacquisizione.keys()):
        d['dt'] = dataacquisizione[dt]
        d['t'] = trazione[i]
        d['1'] = asp1[i]
        d['2'] = asp2[i]
        d['d'] = spazz1[i]
        d['s'] = spazz2[i]
        d['v'] = vbatt[i]
        d['a'] = abatt[i]
        lst['d'][i] = copy.deepcopy(d)

    o = {}
    for i, mac in enumerate(mac_op.keys()):
        o['i'] = date_start_op[i] if i in date_start_op else 0
        o['e'] = date_stop_op[i] if i in date_stop_op else 0
        o['m'] = mac_op[i] if i in mac_op else 0
        lst['o'][i] = copy.copy(o)

    lst['ks'] = copy.copy(turnOn_date)
    lst['ke'] = copy.copy(turnOff_date)

    return json.dumps(lst)


print('****************************')
print('*  Start Fleettrack v 2.3  *')
print('****************************')

print(GetDateTime())

GPIO.add_event_detect(27, GPIO.RISING, callback=addLamp, bouncetime=400)
GPIO.add_event_detect(20, GPIO.BOTH, callback=turnKey, bouncetime=4000)

print('setKeyPos(0)... contatto chiuso...')
keypos.setKeyPos(0)
time.sleep(1)
print('setKeyPos(1)... contatto aperto...')
keypos.setKeyPos(1)
time.sleep(1)
print('setKeyPos(0)... contatto chiuso...')
keypos.setKeyPos(0)
print('Done !')

print('Disconnect...')
location.Disconnect()
print('Done !')
time.sleep(5)
print('SwitchSerialMode...')
huawei_e303.SwitchSerialMode()
print('Done !')
time.sleep(5)

print('Create Scanner Bluetooth Low Energy')
scanner = Scanner().withDelegate(ScanDelegate())
print('Done !')
time.sleep(5)
imeiidx = 0

print('GetImei')
while imei == 0:
    if imeiidx > 10:
        os.system('sudo shutdown -r now')
    imeiidx = imeiidx + 1
    imei = huawei_e303.GetIMEI()
    print('imei = ' + str(imei))

# ATTENZIONE TOGLIERE RIGA SEGUENTE
# imei = 123456789

print('******************')
print('* Connessione... *')
print('******************')
# se non c'e' una connessione attiva , lancia wvdial
'''
response = os.system("ping -c 1 www.google.it")
if response != 0:
    print('Connect...')
    location.Connect()
else:
    print('Already Connected')
'''
location.Connect()
print('*****************************************')
print('* Download MacAddress Beacons operatori *')
print('*****************************************')

my_devices = get_data.macBeacons(True, imei)

print('**************************')
print('* Download Range sensori *')
print('**************************')

sensor_scale = get_data.sensorScale(True, imei)

print('Disconnect...')
location.Disconnect()
print('Done !')
time.sleep(5)

print('************************')
print('* Inizio Ciclo Lettura *')
print('************************')
dateon = GetDateTime()
first_time = True
print 'Date On = ' + dateon

# datestart rappresenta l'istante di inizio lettura
# quello da utilizzare nel calcolo dell'istante di invio dati
datestart = time.time()

while True:
    print('Disconnect...')
    location.Disconnect()
    print('Done !')
    if first_time:
        # Reset dateon
        dateon = GetDateTime()
        # Temperatura
        Temp = round(temp.read_temp(), 1)
        # Livello Elettrolita
        Level = ReadLevel()
        first_time = False
        print 'First Time = False, Date On = ' + dateon
        sending = False

    # n rappresenta il numero di iterazioni prima dell'invio
    print('n = ' + str(n))

    # Lettura dei valori dai sensori
    # Bluetooth
    # time.sleep(15)
    try:
        devices = scanner.scan(180)  # 3 min
    except Exception as e:
        print('scanner.scan() ERROR - {}'.format(traceback.format_exc(e)))
    # Data acquisizione dati
    dataacquisizione[n] = GetDateTime()
    # 0x4a
    trazione[n] = round(sensors_calibrazione.ReadTrazione(float(sensor_scale[2])), 1)
    spazz1[n] = round(sensors_calibrazione.ReadSpazzolaDx(float(sensor_scale[5])), 1)
    spazz2[n] = round(sensors_calibrazione.ReadSpazzolaSx(float(sensor_scale[6])), 1)
    vbatt[n] = round(sensors_calibrazione.ReadVolBatt(), 1)
    # 0x4b
    asp1[n] = round(sensors_calibrazione.ReadAspirazione1(float(sensor_scale[3])), 1)
    asp2[n] = round(sensors_calibrazione.ReadAspirazione2(float(sensor_scale[4])), 1)
    null = round(sensors_calibrazione.Read(1, 1), 1)
    abatt[n] = round(sensors_calibrazione.ReadAmpereBatt(float(sensor_scale[8])), 1)

    # if debug:
    #    # 0x4a
    #    trazione[n] = n
    #    spazz1[n] = n
    #    spazz2[n] = n
    #    vbatt[n] = n
    # 0x4b
    #    asp1[n] = n
    #    asp2[n] = n
    #    abatt[n] = n

    print('Data = ' + str(dataacquisizione[n]))
    print('Trazione = ' + str(trazione[n]))
    print('SpazzolaDx = ' + str(spazz1[n]))
    print('SpazzolaSx = ' + str(spazz2[n]))
    print('Asp1 = ' + str(asp1[n]))
    print('Asp2 = ' + str(asp2[n]))
    print('Vbatt = ' + str(vbatt[n]))
    print('Abatt = ' + str(abatt[n]))
    print('Temp = ' + str(Temp))
    print('*')
    print('Level = ' + str(Level))
    print('Errore = ' + str(nlamp))
    print('StatoChiave = ' + str(GPIO.input(20)))
    print('date_start_op = ' + str(date_start_op))
    print('date_stop_op = ' + str(date_stop_op))
    print('mac_op = ' + str(mac_op))
    print('turnOn_date = ' + str(turnOn_date))
    print('turnOff_date = ' + str(turnOff_date))
    print('*')

    n += 1

    delta_t = int(time.time() - datestart)
    print('delta_t = ' + str(delta_t))
    if ninvii > 5:
        w = 30
    else:
        w = 1

    if delta_t >= (60 * nminutiupload * w):

        print(GetDateTime())

        print('***********************************')
        print('*  Connessione e Localizzazione   *')
        print('***********************************')
        sending = True
        tl = time.time()

        # if debug is False :
        if True:
            print('getLocation')
            try:
                pos = location.getLocation()
            except Exception as e:
                print('location.getLocation() ERROR - {}'.format(traceback.format_exc(e)))
                pos = 'no location detected'

            if pos != 'no location detected':
                jsonData = json.loads(pos)
                if 'location' in jsonData.keys():
                    data_loc = jsonData["location"]
                    lati = data_loc["lat"]
                    longi = data_loc["lng"]
                else:
                    lati = 0
                    longi = 0
            else:
                lati = -1
                longi = -1
        else:
            print('Demo no getLocation')
            lati = -1
            longi = -1

        print('Done...')
        print('Tempo impiegato = ' + str(time.time() - tl) + 'sec')
        print('Latitudine = ' + str(lati))
        print('Longitudine = ' + str(longi))

        print('************************************')
        print('* Download Blocco E Sincro OROLOGIO*')
        print('************************************')

        # SINCRONIZZAZIONE OROLOGIO CON NTP
        os.system("sudo systemctl enable ntp")
        os.system("sudo timedatectl set-ntp 1")

        bloccoStatus = get_data.getBlocco(debug, imei)

        if bloccoStatus == '1':
            keypos.setKeyPos(1)
        if bloccoStatus == '0':
            keypos.setKeyPos(0)

        print('***********************')
        print('* Creazione JSON      *')
        print('***********************')

        json_data = CreateJsonData(dateon, imei, lati, longi, Temp, Level, nlamp)
        print('JSON = ' + json_data)

        print('***********************')
        print('* Compressione        *')
        print('***********************')

        json_str = str(json_data).encode("utf8")
        json_str_zlib = zlib.compress(json_str, 9)

        print('JSON Length = ' + str(len(json_str)))
        print('JSON Zlib Length = ' + str(len(json_str_zlib)))

        print('******************')
        print('* Invio Dati     *')
        print('******************')
        uploadFile.sendData(json_str_zlib)
        ninvii += 1
        print('Disconnect...')
        location.Disconnect()
        print('Done !')

        print('******************')
        print('* Reset Dati     *')
        print('******************')

        datestart = time.time()
        first_time = True
        n = 0
        date_start_op = {}
        date_stop_op = {}
        mac_op = {}
        turnOn_date = []
        turnOff_date = []
