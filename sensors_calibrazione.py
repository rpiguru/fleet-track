import Adafruit_ADS1x15
import time

adc1 = Adafruit_ADS1x15.ADS1115(address=0x4a, busnum=1)
adc2 = Adafruit_ADS1x15.ADS1115(address=0x4b, busnum=1)

GAIN = 2 / 3
Resolution = (6.144 * 2.0) / 65536.0
s8 = 80.0
s2 = 200.0

Vt0 = 0
Vsd = 0
Vss = 0
Va1 = 0
Va2 = 0
Vab = 0

print('Calibrazione...')
time.sleep(60)
for count in range(1, 20):
    Vt0 = adc1.read_adc(0, gain=GAIN) * Resolution
    Vsd = adc1.read_adc(1, gain=GAIN) * Resolution
    Vss = adc1.read_adc(2, gain=GAIN) * Resolution
    Va1 = adc2.read_adc(0, gain=GAIN) * Resolution
    Va2 = adc2.read_adc(1, gain=GAIN) * Resolution
    Vab = adc2.read_adc(3, gain=GAIN) * Resolution
print('DONE.')


# ADC1 0x4a
def ReadTrazione(s):
    val = adc1.read_adc(0, gain=GAIN)
    vr = val * Resolution
    v_out = (vr - Vt0) * s
    return v_out


def ReadSpazzolaDx(s):
    val = adc1.read_adc(1, gain=GAIN)
    vr = val * Resolution
    v_out = (vr - Vsd) * s
    return v_out


def ReadSpazzolaSx(s):
    val = adc1.read_adc(2, gain=GAIN)
    vr = val * Resolution
    v_out = (vr - Vss) * s
    return v_out


def ReadVolBatt():
    val = adc1.read_adc(3, gain=GAIN)
    v_out = (val * Resolution) * 151.0
    return v_out


#  ADC2 0x4b
def ReadAspirazione1(s):
    val = adc2.read_adc(0, gain=GAIN)
    vr = val * Resolution
    v_out = (vr - Va1) * s
    return v_out


def ReadAspirazione2(s):
    val = adc2.read_adc(1, gain=GAIN)
    vr = val * Resolution
    v_out = (vr - Va2) * s
    return v_out


def Read(v0, s):
    val = adc2.read_adc(2, gain=GAIN)
    vr = val * Resolution
    v_out = (vr - v0) * s
    return v_out


def ReadAmpereBatt(s):
    val = adc2.read_adc(3, gain=GAIN)
    vr = val * Resolution
    v_out = (vr - Vab) * s
    return v_out
